<?php

/**
 * @file
 * The spin module's profile settings.
 */

const SPIN_AUTOSPIN_MAX = 70000;
const SPIN_AUTOSPIN_MIN = 5000;
const SPIN_AUTOSPIN_INC = 5000;
const SPIN_EFFECT_MAX = 1000;
const SPIN_EFFECT_MIN = 200;
const SPIN_EFFECT_INC = 50;

/**
 * The spin/slideshow profile delete form.
 */
function spin_admin_delete_form($form, &$state, $sid = 0) {
  $form['#submit'] = array('spin_admin_delete_form_submit');
  $form['sid'] = array(
    '#type'  => 'hidden',
    '#value' => $sid ? $sid : '',
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit'),
  );
  $form['warning'] = array(
    '#type'   => 'markup',
    '#markup' => '<p>' . t('Delete this profile? This action cannot be undone.') . '</p>',
  );
  return $form;
}

/**
 * Custom profile delete hook.
 */
function spin_admin_delete_form_submit($form, &$state) {
  return !empty($state['values']['sid']) ? SpinStorage::deleteSpin($state['values']['sid']) : NULL;
}

/**
 * The spin/slideshow add/edit profile form.
 */
function spin_admin_edit_form($form, &$state, $type = '', $sid = 0) {
  $header = '<h2 style="background-color:#eeeeee; border:1px #aaaaaa solid; margin:1.0rem auto 0.1rem 0; padding:0.5rem; width:50%">@txt</h2>';
  $opts = array(
    'true'  => t('Yes'),
    'false' => t('No'),
  );
  $form['#submit'] = array('spin_admin_edit_form_submit');
  $form['#validate'] = array('spin_admin_edit_form_validate');

  $form['sid'] = array(
    '#type'  => 'value',
    '#value' => $sid ? $sid : '',
  );
  $form['label'] = array(
    '#title'         => t('Label'),
    '#type'          => 'textfield',
    '#default_value' => $sid ? SpinStorage::getLabel($sid) : '',
    '#size'          => 35,
    '#maxlength'     => 32,
    '#required'      => TRUE,
  );
  $form['name'] = array(
    '#title'         => t('Machine Name'),
    '#type'          => 'machine_name',
    '#description'   => t('Lowercase letters and underscore, (_) only.'),
    '#default_value' => $sid ? SpinStorage::getName($sid) : '',
    '#size'          => 35,
    '#maxlength'     => 32,
    '#required'      => TRUE,
    '#disabled'      => (bool) $sid,
    '#machine_name'  => array(
      'replace_pattern' => '[^a-z0-9_]+',
      'replace'         => '_',
      'source'          => array('label'),
    ),
  );
  switch ($type) {
    case 'slideshow':
      _spin_admin_add_slideshow_elements($form, $header, $opts);
      break;

    case 'spin':
      _spin_admin_add_spin_elements($form, $header, $opts);
      break;

    default:
      return array();
  }
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Custom submit hook.
 */
function spin_admin_edit_form_submit($form, &$state) {
  $options = SpinHelper::extractOptions($state['values']);

  return SpinHelper::mergeSpin($state['values'], $options);
}

/**
 * Custom validation hook.
 */
function spin_admin_edit_form_validate($form, &$state) {
  if (!preg_match('/^[a-zA-z][\w -]*[a-zA-z0-9]$/', $state['values']['label'])) {
    form_set_error('label', t('Illegal character in label.'));
  }
}

/**
 * The spin admin profile list.
 */
function spin_admin_list_form($form, &$state) {
  $opts = array('query' => drupal_get_destination());
  $data = array(
    'caption' => t('Slideshow Profiles'),
    'header' => array(
      t('Profile'),
      t('Type'),
      t('Delete'),
      t('Edit'),
    ),
    'rows' => array(),
  );
  foreach (SpinStorage::getList('slideshow') as $obj) {
    $data['rows'][] = array(
      $obj->label,
      $obj->type,
      ($obj->name == 'default') ? '&nbsp;' : l(t('Delete'), "admin/config/spin/delete/$obj->sid", $opts),
      l(t('Edit'), "admin/config/spin/edit/$obj->type/$obj->sid", $opts),
    );
  }
  $form['links'] = array(
    '#type'   => 'markup',
    '#markup' => '<p>' . l(t('Add Spin Profile'), 'admin/config/spin/add/spin', $opts) . "</p>\n<p>" . l(t('Add Slideshow Profile'), 'admin/config/spin/add/slideshow', $opts) . '</p>',
  );
  $form['list_slideshow'] = array(
    '#type'   => 'markup',
    '#markup' => theme('table', $data),
  );
  $data['caption'] = t('Spin Profiles');
  $data['rows'] = array();

  foreach (SpinStorage::getList('spin') as $obj) {
    $data['rows'][] = array(
      $obj->label,
      $obj->type,
      ($obj->name == 'default') ? '&nbsp;' : l(t('Delete'), "admin/config/spin/delete/$obj->sid", $opts),
      l(t('Edit'), "admin/config/spin/edit/$obj->type/$obj->sid", $opts),
    );
  }
  $form['list_spin'] = array(
    '#type'   => 'markup',
    '#markup' => theme('table', $data),
  );
  return $form;
}

/**
 * Add the slideshow elements to the form.
 */
function _spin_admin_add_slideshow_elements(&$form, $header, $opts) {
  $data = !empty($form['sid']['#value']) ? SpinHelper::getData($form['sid']['#value'], 'slideshow') : SpinHelper::getDefaultData('slideshow');

  $form['name']['#machine_name']['exists'] = '_spin_admin_slideshow_name_exists';

  $form['type'] = array(
    '#type'  => 'value',
    '#value' => 'slideshow',
  );
  $form['head_common'] = array(
    '#type'   => 'markup',
    '#markup' => str_replace('@txt', t('Common'), $header),
  );
  $form['spin_orientation'] = array(
    '#title'         => t('Orientation'),
    '#description'   => t('Direction for slides (left/right or up/down).'),
    '#type'          => 'select',
    '#default_value' => isset($data['orientation']) ? $data['orientation'] : 'horizontal',
    '#options'       => array(
      'horizontal' => t('Horizontal'),
      'vertical'   => t('Vertical'),
    ),
  );
  $form['spin_arrows'] = array(
    '#title'         => t('Arrows'),
    '#description'   => t('Show navigation arrows on hover.'),
    '#type'          => 'select',
    '#default_value' => isset($data['arrows']) ? $data['arrows'] : 'true',
    '#options'       => $opts,
  );
  $form['spin_loop'] = array(
    '#title'         => t('Loop'),
    '#description'   => t('Continue slideshow after last slide.'),
    '#type'          => 'select',
    '#default_value' => isset($data['loop']) ? $data['loop'] : 'false',
    '#options'       => $opts,
  );
  $form['spin_effect'] = array(
    '#title'         => t('Effect'),
    '#description'   => t('Slide transition effect.'),
    '#type'          => 'select',
    '#default_value' => isset($data['effect']) ? $data['effect'] : 'slide',
    '#options'       => array(
      'bars3d'       => t('Bars3d'),
      'blinds3d'     => t('Blinds3d'),
      'blocks'       => t('Blocks'),
      'cube'         => t('Cube'),
      'diffusion'    => t('Diffusion'),
      'dissolve'     => t('Dissolve'),
      'fade'         => t('Fade'),
      'fade-down'    => t('Down'),
      'fade-up'      => t('Up'),
      'flip'         => t('Flip'),
      'random'       => t('Random'),
      'slide'        => t('Slide'),
      'slide-change' => t('Change'),
      'slide-in'     => t('In'),
      'slide-out'    => t('Out'),
    ),
  );
  $form['spin_effect-speed'] = array(
    '#title'         => t('Effect Speed'),
    '#description'   => t('Speed of slide transition effect.'),
    '#type'          => 'select',
    '#default_value' => isset($data['effect-speed']) ? $data['effect-speed'] : 600,
    '#options'       => array_combine(range(SPIN_EFFECT_MIN, SPIN_EFFECT_MAX, SPIN_EFFECT_INC), range(SPIN_EFFECT_MIN, SPIN_EFFECT_MAX, SPIN_EFFECT_INC)),
  );
  $form['head_autoplay'] = array(
    '#type'   => 'markup',
    '#markup' => str_replace('@txt', t('Autoplay'), $header),
  );
  $form['spin_autoplay'] = array(
    '#title'         => t('Autoplay'),
    '#description'   => t('Autoplay slideshow after initialization.'),
    '#type'          => 'select',
    '#default_value' => isset($data['autoplay']) ? $data['autoplay'] : 'false',
    '#options'       => $opts,
  );
  $form['spin_shuffle'] = array(
    '#title'         => t('Shuffle'),
    '#description'   => t('Shuffle slide order.'),
    '#type'          => 'select',
    '#default_value' => isset($data['shuffle']) ? $data['shuffle'] : 'false',
    '#options'       => $opts,
  );
  $form['spin_pause'] = array(
    '#title'         => t('Pause'),
    '#description'   => t('Click to pause slideshow.'),
    '#type'          => 'select',
    '#default_value' => isset($data['pause']) ? $data['pause'] : 'false',
    '#options'       => $opts,
  );
  $form['head_selectors'] = array(
    '#type'   => 'markup',
    '#markup' => str_replace('@txt', t('Selectors'), $header),
  );
  $form['spin_selectors'] = array(
    '#title'         => t('Selectors'),
    '#description'   => t('Where to show selectors.'),
    '#type'          => 'select',
    '#default_value' => isset($data['selectors']) ? $data['selectors'] : 'bottom',
    '#options'       => array(
      'left'   => t('Left'),
      'top'    => t('Top'),
      'right'  => t('Right'),
      'bottom' => t('Bottom'),
      'none'   => t('None'),
    ),
  );
  $form['spin_selectors-style'] = array(
    '#title'         => t('Selectors Style'),
    '#description'   => t('Style of selectors.'),
    '#type'          => 'select',
    '#default_value' => isset($data['selectors-style']) ? $data['selectors-style'] : 'thumbnails',
    '#options'       => array(
      'bullets'    => t('Bullets'),
      'thumbnails' => t('Thumbnails'),
    ),
  );
  $form['spin_selectors-eye'] = array(
    '#title'         => t('Selectors Eye'),
    '#description'   => t('Highlighter for active thumbnail.'),
    '#type'          => 'select',
    '#default_value' => isset($data['selectors-eye']) ? $data['selectors-eye'] : 'false',
    '#options'       => $opts,
  );
  $form['head_caption'] = array(
    '#type'   => 'markup',
    '#markup' => str_replace('@txt', t('Caption'), $header),
  );
  $form['spin_caption'] = array(
    '#title'         => t('Caption'),
    '#description'   => t('Display text on a slide.'),
    '#type'          => 'select',
    '#default_value' => isset($data['caption']) ? $data['caption'] : 'false',
    '#options'       => $opts,
  );
  $form['spin_caption-effect'] = array(
    '#title'         => t('Caption Effect'),
    '#description'   => t('Caption change with effect.'),
    '#type'          => 'select',
    '#default_value' => isset($data['caption-effect']) ? $data['caption-effect'] : 'fade',
    '#options'       => array(
      'fade'     => t('Fade'),
      'dissolve' => t('Dissolve'),
      'fixed'    => t('Fixed'),
    ),
  );
  $form['head_other'] = array(
    '#type'   => 'markup',
    '#markup' => str_replace('@txt', t('Other'), $header),
  );
  $form['spin_fullscreen'] = array(
    '#title'         => t('Fullscreen'),
    '#description'   => t('Full screen.'),
    '#type'          => 'select',
    '#default_value' => isset($data['fullscreen']) ? $data['fullscreen'] : 'true',
    '#options'       => $opts,
  );
  $form['spin_keyboard'] = array(
    '#title'         => t('Keyboard'),
    '#description'   => t('Keyboard navigation.'),
    '#type'          => 'select',
    '#default_value' => isset($data['keyboard']) ? $data['keyboard'] : 'false',
    '#options'       => $opts,
  );
  $form['spin_preload'] = array(
    '#title'         => t('Preload'),
    '#description'   => t('Load images on demand.'),
    '#type'          => 'select',
    '#default_value' => isset($data['preload']) ? $data['preload'] : 'true',
    '#options'       => $opts,
  );
  $form['spin_loader'] = array(
    '#title'         => t('Loader'),
    '#description'   => t('Show loading icon.'),
    '#type'          => 'select',
    '#default_value' => isset($data['loader']) ? $data['loader'] : 'true',
    '#options'       => $opts,
  );
  $form['spin_links'] = array(
    '#title'         => t('Links'),
    '#description'   => t('Open links associated with image slides.'),
    '#type'          => 'select',
    '#default_value' => isset($data['links']) ? $data['links'] : '',
    '#options'       => array(
      'false'  => t('No'),
      '_self'  => t('In the same page'),
      '_blank' => t('In a new tab/window'),
    ),
  );
}

/**
 * Add the spin elements to the form.
 */
function _spin_admin_add_spin_elements(&$form, $header, $opts) {
  $data = !empty($form['sid']['#value']) ? SpinHelper::getData($form['sid']['#value'], 'spin') : SpinHelper::getDefaultData('spin');

  $form['name']['#machine_name']['exists'] = '_spin_admin_spin_name_exists';

  $form['type'] = array(
    '#type'  => 'value',
    '#value' => 'spin',
  );
  $form['head_autospin'] = array(
    '#type'   => 'markup',
    '#markup' => str_replace('@txt', t('Autospin'), $header),
  );
  $form['spin_autospin'] = array(
    '#title'         => t('Autospin'),
    '#description'   => t('Automatically spin the image.'),
    '#type'          => 'select',
    '#default_value' => isset($data['autospin']) ? $data['autospin'] : 'once',
    '#options'       => array(
      'off'      => t('Off'),
      'once'     => t('Once'),
      'twice'    => t('Twice'),
      'infinite' => t('infinite'),
    ),
  );
  $form['spin_autospinSpeed'] = array(
    '#title'         => t('Autospin Speed'),
    '#description'   => t('The time (ms) taken to complete 1 rotation.'),
    '#type'          => 'select',
    '#default_value' => isset($data['autospinSpeed']) ? $data['autospinSpeed'] : 3500,
    '#options'       => array_combine(range(SPIN_AUTOSPIN_MIN, SPIN_AUTOSPIN_MAX, SPIN_AUTOSPIN_INC), range(SPIN_AUTOSPIN_MIN, SPIN_AUTOSPIN_MAX, SPIN_AUTOSPIN_INC)),
  );
  $form['spin_autospinStart'] = array(
    '#title'         => t('Autospin Start'),
    '#description'   => t('Start autospin on page load, click or hover.'),
    '#type'          => 'select',
    '#default_value' => isset($data['autospinStart']) ? $data['autospinStart'] : 'load',
    '#options'       => array(
      'click' => t('Click'),
      'hover' => t('Hover'),
      'load'  => t('Load'),
    ),
  );
  $form['spin_autospinStop'] = array(
    '#title'         => t('Autospin Stop'),
    '#description'   => t('Stop autospin on click, hover or never.'),
    '#type'          => 'select',
    '#default_value' => isset($data['autospinStop']) ? $data['autospinStop'] : 'click',
    '#options'       => array(
      'click' => t('Click'),
      'hover' => t('Hover'),
      'never' => t('Never'),
    ),
  );
  $form['spin_autospinDirection'] = array(
    '#title'         => t('Autospin Direction'),
    '#description'   => t('Direction of spin.'),
    '#type'          => 'select',
    '#default_value' => isset($data['autospinDirection']) ? $data['autospinDirection'] : 'clockwise',
    '#options'       => array(
      'clockwise'     => t('Clockwise'),
      'anticlockwise' => t('Anticlockwise'),
    ),
  );
  $form['head_mouse'] = array(
    '#type'   => 'markup',
    '#markup' => str_replace('@txt', t('Mouse'), $header),
  );
  $form['spin_spin'] = array(
    '#title'         => t('Spin Method'),
    '#description'   => t('Method for spinning the image.'),
    '#type'          => 'select',
    '#default_value' => isset($data['spin']) ? $data['spin'] : 'drag',
    '#options'       => array(
      'drag'  => t('Drag'),
      'hover' => t('Hover'),
    ),
  );
  $form['spin_speed'] = array(
    '#title'         => t('Drag Speed'),
    '#description'   => t('Speed of spin while dragging (100 = fast).'),
    '#type'          => 'select',
    '#default_value' => isset($data['speed']) ? $data['speed'] : 50,
    '#options'       => array_combine(range(0, 100, 5), range(0, 100, 5)),
  );
  $form['spin_zoom'] = array(
    '#title'         => t('Zoom'),
    '#description'   => t('Zoom level on click (0 = disabled).'),
    '#type'          => 'select',
    '#default_value' => isset($data['zoom']) ? $data['zoom'] : 3,
    '#options'       => array_combine(range(0, 10), range(0, 10)),
  );
  $form['spin_rightClick'] = array(
    '#title'         => t('Right Click'),
    '#description'   => t('Show right-click menu on the image.'),
    '#type'          => 'select',
    '#default_value' => isset($data['rightClick']) ? $data['rightClick'] : 'false',
    '#options'       => $opts,
  );
  $form['head_misc'] = array(
    '#type'   => 'markup',
    '#markup' => str_replace('@txt', t('Misc'), $header),
  );
  $form['spin_fullscreen'] = array(
    '#title'         => t('Fullscreen'),
    '#description'   => t('Enable full-screen spin if large images exist.'),
    '#type'          => 'select',
    '#default_value' => isset($data['fullscreen']) ? $data['fullscreen'] : 'true',
    '#options'       => $opts,
  );
  $form['spin_initializeOn'] = array(
    '#title'         => t('Image Download'),
    '#description'   => t('When to download the images.'),
    '#type'          => 'select',
    '#default_value' => isset($data['initializeOn']) ? $data['initializeOn'] : 'load',
    '#options'       => array(
      'click' => t('Click'),
      'hover' => t('Hover'),
      'load'  => t('Load'),
    ),
  );
}

/**
 * Determine if the slideshow profile name exists.
 *
 * @param string $name
 *   The profile name.
 *
 * @return bool
 *   True if the name exists.
 */
function _spin_admin_slideshow_name_exists($name) {
  return SpinStorage::nameExists($name, 'slideshow');
}

/**
 * Determine if the spin profile name exists.
 *
 * @param string $name
 *   The profile name.
 *
 * @return bool
 *   True if the name exists.
 */
function _spin_admin_spin_name_exists($name) {
  return SpinStorage::nameExists($name, 'spin');
}
