<?php

/**
 * @file
 * Install file support functions.
 */

/**
 * Insert the default profiles.
 */
function _spin_install_default_profiles() {
  $key = array(
    'name'  => 'default',
    'label' => 'Default',
    'type'  => 'spin',
  );
  $fields = array(
    'profile' => 'autospin=once&autospinDirection=clockwise&autospinSpeed=5000&autospinStart=load&autospinStop=click&fullscreen=true&initializeOn=load&rightClick=false&speed=50&spin=drag&zoom=3',
    'data'    => serialize(array()),
  );
  db_merge('spin')->key($key)->fields($fields)->execute();

  $key = array(
    'name'  => 'default',
    'label' => 'Default',
    'type'  => 'slideshow',
  );
  $fields = array(
    'profile' => 'arrows:true; autoplay:false; caption:false; caption-effect:fade; effect:blocks; effect-speed:600; fullscreen:true; height:100%; keyboard:false; links:false; loader:true; loop:false; orientation:horizontal; pause:false; preload:false; selectors:bottom; selectors-eye:true; selectors-size:50; selectors-style:thumbnails; shuffle:false',
    'data'    => serialize(array()),
  );
  db_merge('spin')->key($key)->fields($fields)->execute();
}

/**
 * Supporting table installation in update hook.
 */
function _spin_install_schema() {
  return array(
    'description' => 'Spin and slideshow display profiles.',
    'fields'      => array(
      'sid' => array(
        'description' => "The spin's ID.",
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'name' => array(
        'description' => 'The machine name of the profile',
        'type'        => 'varchar',
        'length'      => 32,
        'not null'    => TRUE,
      ),
      'type' => array(
        'description' => 'The profile type, ("spin" or "slideshow").',
        'type'        => 'varchar',
        'length'      => 32,
        'not null'    => TRUE,
      ),
      'label' => array(
        'description' => 'The profile label.',
        'type'        => 'varchar',
        'length'      => 32,
        'not null'    => TRUE,
      ),
      'profile' => array(
        'description' => 'The display option string.',
        'type'        => 'blob',
        'size'        => 'normal',
        'serialize'   => FALSE,
      ),
      'data' => array(
        'description' => 'Serialized display option data.',
        'type'        => 'blob',
        'size'        => 'normal',
        'serialize'   => TRUE,
      ),
    ),
    'indexes' => array(
      'name' => array('name'),
      'type' => array('type'),
    ),
    'unique keys' => array(
      'name_type' => array('name', 'type'),
    ),
    'primary key' => array('sid'),
  );
}
