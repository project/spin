<?php

/**
 * @file
 * Template implementation to display the spin field.
 */
?>
<div class="field-spin-slideshow">

<?php if (count($data->spins) > 1): ?>
  <div class="MagicSlideshow multi-spins spin-slideshow" data-options="<?php print $data->slideshow_options; ?>">
<?php else: ?>
  <div class="NoSlideshow spin-slideshow">
<?php endif; ?>

  <?php foreach ($data->spins as $i => $obj): ?>
    <div data-thumb-image="<?php print $obj->nav; ?>">

    <?php if ($obj->spin): ?>
      <div class="Sirv spin-slide spin-slide-<?php print $i; ?>" data-src="<?php print $obj->spin . (strpos('?', $obj->spin) ? '' : $data->spin_query); ?>">
      </div>
    <?php else: ?>
      <img class="spin-slide spin-slide-<?php print $i; ?>" src="<?php print $obj->img; ?>" />
    <?php endif; ?>

      <span class="loading"></span>
    </div>
  <?php endforeach; ?>

  </div>
</div>
