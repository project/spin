<?php

/**
 * @file
 * Contains the SpinData class.
 *
 * Builds data sets for pages and blocks. Uses the SpinCache class for caching.
 */

/**
 * Build data structures to be rendered.
 */
class SpinData {

  /**
   * Build the data to display the spins.
   *
   * @param object $data
   *   The data to display the spin slideshow.
   * @param array $items
   *   An array of spin objects to display.
   *
   * @return object
   *   The data to display the spin slideshow.
   */
  public static function getSpin($data, array $items) {
    list($cid, $cache) = SpinCache::get(__CLASS__, __FUNCTION__, $data, $items);

    if ($cache !== FALSE) {
      return $cache;
    }
    foreach ($items as $item) {
      $data->spins[] = (object) array(
        'img'  => SpinHelper::getThumb($item['fid'], 'spin_img'),
        'nav'  => SpinHelper::getThumb($item['fid'], 'spin_nav'),
        'spin' => $item['spin'],
      );
    }
    drupal_alter('spin_data', $data);

    return SpinCache::set($cid, array('data' => $data));
  }

}
