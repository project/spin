<?php

/**
 * @file
 * Contains the SpinHelper class.
 *
 * General private and helper methods. If it's not a hook, SQL or cacheable data
 * for pages/blocks then it probably goes here.
 */

/**
 * Helper class.
 */
class SpinHelper {

  /**
   * Extract the spin options from the form.
   *
   * @param array $values
   *   The form values array.
   *
   * @return array
   *   An array of spin options.
   */
  public static function extractOptions(array $values) {
    $options = array();

    foreach ($values as $field => $val) {
      if (!preg_match('/^spin_([-\w]+)/', $field, $m)) {
        continue;
      }
      $options[$m[1]] = $val;
    }
    ksort($options);

    return $options;
  }

  /**
   * Fetch the serialized display option data.
   *
   * @param int $sid
   *   The spin ID.
   * @param string $type
   *   The spin profile type, ("slideshow" or "spin").
   *
   * @return string
   *   The serialized display option data.
   */
  public static function getData($sid, $type) {
    $defaults = self::getDataPrototype($type);

    if (empty($defaults)) {
      return array();
    }
    $data = SpinStorage::getData($sid);

    $data = $data ? unserialize($data) : array();

    return is_array($data) ? $data + $defaults : $defaults;
  }

  /**
   * Fetch the serialized display option data.
   *
   * @param string $type
   *   The spin profile type, ("slideshow" or "spin").
   *
   * @return string
   *   The serialized display option data.
   */
  public static function getDefaultData($type) {
    $defaults = self::getDataPrototype($type);

    if (empty($defaults)) {
      return array();
    }
    $data = SpinStorage::getDefaultData($type);

    $data = $data ? unserialize($data) : array();

    return is_array($data) ? $data + $defaults : $defaults;
  }

  /**
   * Retrive the image URL.
   *
   * @param int $fid
   *   The file ID.
   * @param array $element
   *   An element array.
   * @param string $style
   *   The image style.
   *
   * @return string
   *   A themed preview image.
   */
  public static function getPreview($fid, array &$element = array(), $style = 'thumbnail') {
    $file = file_load($fid);

    if (isset($element['#value']['width']) && isset($element['#value']['height'])) {
      $info = array(
        'width'  => $element['#value']['width'],
        'height' => $element['#value']['height'],
      );
    }
    elseif (!empty($file->uri)) {
      $info = image_get_info($file->uri);
    }
    $img = array(
      'style_name' => $style,
      'path'       => !empty($file->uri) ? $file->uri : '',
      'width'      => !empty($info['width']) ? $info['width'] : '',
      'height'     => !empty($info['height']) ? $info['height'] : '',
      'attributes' => array('id' => "spin-preview-img-$fid"),
    );
    $element['width'] = array(
      '#type'  => 'hidden',
      '#value' => $img['width'],
    );
    $element['height'] = array(
      '#type'  => 'hidden',
      '#value' => $img['height'],
    );
    return !empty($file->uri) ? theme('image_style', $img) : '';
  }

  /**
   * Build a string of data-options.
   *
   * @return string
   *   A string of Magic Slideshow data options.
   */
  public static function getSlideshowDataOptions(array $options = array()) {
    $attribute = '';
    $options += self::getDataPrototype('slideshow');

    ksort($options);

    foreach ($options as $key => $val) {
      $attribute .= $attribute ? "; $key:$val" : "$key:$val";
    }
    return $attribute;
  }

  /**
   * Build a string of data-options.
   *
   * @return string
   *   A string of Magic Slideshow data options.
   */
  public static function getSpinQueryString(array $options = array()) {
    $options += self::getDataPrototype('spin');
    $query = '';

    ksort($options);

    foreach ($options as $key => $val) {
      $query .= $query ? "&$key=$val" : "$key=$val";
    }
    return $query;
  }

  /**
   * Retrive the image URL.
   *
   * @param int $fid
   *   The file ID.
   * @param string $style
   *   The image style.
   *
   * @return string
   *   The URL to the image.
   */
  public static function getThumb($fid, $style) {
    $file = file_load($fid);

    if (!$file->status) {
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);

      if (arg(0) == 'node' && is_numeric(arg(1))) {
        file_usage_add($file, 'spin', 'node', arg(1));
      }
      $file = file_load($fid);
    }
    $style_uri = !empty($file->uri) ? image_style_path($style, $file->uri) : '';

    if (!$style_uri) {
      return '';
    }
    if (!file_exists($style_uri)) {
      image_style_create_derivative(image_style_load($style), $file->uri, $style_uri);
    }
    return file_exists($style_uri) ? image_style_url($style, $file->uri) : file_create_url($file->uri);
  }

  /**
   * Retrive the image URL.
   *
   * @param array $values
   *   The form values array.
   * @param array $options
   *   The profile options.
   *
   * @return Drupal\Core\Database\Query\InsertQuery|Update
   *   An InsertQuery or Update object for this connection.
   */
  public static function mergeSpin(array $values, array $options) {
    $profile = ($values['type'] == 'spin') ? SpinHelper::getSpinQueryString($options) : SpinHelper::getSlideshowDataOptions($options);

    if (!empty($values['sid'])) {
      $fields = array(
        'label'   => $values['label'],
        'profile' => $profile,
        'data'    => serialize($options),
      );
      return SpinStorage::updateSpin($values['sid'], $fields);
    }
    else {
      $fields = array(
        'name'    => $values['name'],
        'type'    => $values['type'],
        'label'   => $values['label'],
        'profile' => $profile,
        'data'    => serialize($options),
      );
      return SpinStorage::createSpin($fields);
    }
  }

  /**
   * Scan theme and module directories for the templates.
   *
   * @param string $module
   *   The module name, (without the suffix).
   * @param string $template
   *   The template name, (without the suffixes).
   *
   * @return string
   *   Drupal path to the template's directory, (default .../module/templates).
   */
  public static function tplPath($module, $template = '') {
    $theme_path = drupal_get_path('theme', variable_get('theme_default', 'garland'));

    $scan = file_scan_directory($theme_path, "/^$template\.tpl\.php$/", array('recurse' => TRUE, 'filename' => TRUE));

    if (!count($scan)) {
      $scan = file_scan_directory(drupal_get_path('module', $module), "/^$template\.tpl\.php$/", array('recurse' => TRUE, 'filename' => TRUE));
    }
    return count($scan) ? preg_replace('/(\/[^\/]+)$/', '', key($scan)) : drupal_get_path('module', $module) . '/templates';
  }

  /**
   * Fetch an array of default display option data.
   *
   * @param string $type
   *   The spin profile type, ("slideshow" or "spin").
   *
   * @return string
   *   An array of default display option data.
   */
  protected static function getDataPrototype($type = '') {
    switch ($type) {
      case 'slideshow':
        return array(
          'arrows'          => 'true',
          'autoplay'        => 'false',
          'caption'         => 'false',
          'caption-effect'  => 'fade',
          'effect'          => 'blocks',
          'effect-speed'    => '600',
          'fullscreen'      => 'true',
          'keyboard'        => 'false',
          'links'           => 'false',
          'loader'          => 'true',
          'loop'            => 'false',
          'orientation'     => 'horizontal',
          'pause'           => 'false',
          'preload'         => 'false',
          'selectors'       => 'bottom',
          'selectors-eye'   => 'true',
          'selectors-style' => 'thumbnails',
          'shuffle'         => 'false',
        );

      case 'spin':
        return array(
          'autospin'          => 'once',
          'autospinSpeed'     => '5000',
          'autospinStart'     => 'load',
          'autospinStop'      => 'click',
          'autospinDirection' => 'clockwise',
          'spin'              => 'drag',
          'speed'             => '50',
          'zoom'              => '3',
          'rightClick'        => 'false',
          'fullscreen'        => 'true',
          'initializeOn'      => 'load',
        );
    }
    return array();
  }

}
